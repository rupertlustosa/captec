package br.com.rupertlustosa.captec.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.rupertlustosa.captec.R;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private String username = "rupertlustosa@gmail.com";
    private String password = "rupertlustosa";

    private AutoCompleteTextView autoCompleteTextViewEmail;
    private EditText editTextPassword;
    private Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        autoCompleteTextViewEmail = (AutoCompleteTextView) this.findViewById(R.id.email);
        editTextPassword = (EditText) this.findViewById(R.id.password);
        buttonLogin = (Button) this.findViewById(R.id.email_sign_in_button);

        autoCompleteTextViewEmail.setText(this.username);
        editTextPassword.setText(this.password);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String typedUsername = autoCompleteTextViewEmail.getText().toString();
                String typedPassword = editTextPassword.getText().toString();

                /*if(isEmailValid(typedUsername)){

                }*/

                if (typedPassword.equals(password) && typedUsername.equals(username)) {

                    SharedPreferences sharedPreferences = getSharedPreferences("APLICACAO", MODE_PRIVATE);

                    SharedPreferences.Editor sEditor = sharedPreferences.edit();
                    sEditor.putString("username", typedUsername);
                    sEditor.putString("password", typedPassword);
                    sEditor.apply();

                    Log.e("Title", "Cliquei no botão de login digitando " + typedUsername);
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                } else {
                    Log.e("Title", "Errado " + typedUsername);
                    //http://www.devmedia.com.br/exibindo-mensagens-no-android-com-a-classe-toast/26668
                    Toast.makeText(getApplicationContext(), "Login ou senha inválidos", Toast.LENGTH_SHORT).show();
                    //Snackbar.make(, "Login ou senha inválidos").show();
                }
            }
        });

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }
}

