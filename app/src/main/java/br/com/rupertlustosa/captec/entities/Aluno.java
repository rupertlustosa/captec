package br.com.rupertlustosa.captec.entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by rupertlustosa on 21/12/16.
 */

public class Aluno extends RealmObject{
    @PrimaryKey
    private long id;
    private String nome;
    private String nascimento;
}