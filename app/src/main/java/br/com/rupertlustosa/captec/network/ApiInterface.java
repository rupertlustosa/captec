package br.com.rupertlustosa.captec.network;

import java.util.List;

import br.com.rupertlustosa.captec.entities.Escola;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by rupertlustosa on 21/12/16.
 */

public interface ApiInterface {
    @GET("GetSync.php")
    Call<List<Escola>> getEscolas();
}
