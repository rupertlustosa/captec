package br.com.rupertlustosa.captec.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by rupertlustosa on 05/12/16.
 */

public class Turma extends RealmObject {
    @PrimaryKey
    private long id;
    private String turma;
    private String nivel;
    private RealmList<Aluno> alunos;
    // private boolean alterado;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public RealmList<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(RealmList<Aluno> alunos) {
        this.alunos = alunos;
    }

    // private boolean removido;
}
