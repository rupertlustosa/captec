package br.com.rupertlustosa.captec.entities;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by rupertlustosa on 21/12/16.
 */

public class Escola extends RealmObject {
    @PrimaryKey
    private long id;
    private String escola;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEscola() {
        return escola;
    }

    public void setEscola(String escola) {
        this.escola = escola;
    }

    public RealmList<Turma> getTurmas() {
        return turmas;
    }

    public void setTurmas(RealmList<Turma> turmas) {
        this.turmas = turmas;
    }

    private RealmList<Turma> turmas;
}
